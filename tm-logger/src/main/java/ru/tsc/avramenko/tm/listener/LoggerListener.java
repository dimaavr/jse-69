package ru.tsc.avramenko.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.ILoggingService;
import ru.tsc.avramenko.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Service
@NoArgsConstructor
public class LoggerListener implements MessageListener {

    @NotNull
    @Autowired
    private ILoggingService loggingService;

    @Override
    public void onMessage(@NotNull final Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog((TextMessage) message);
        }
    }

}
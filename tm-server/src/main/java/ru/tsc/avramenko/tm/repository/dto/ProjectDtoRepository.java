package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.dto.ProjectDTO;

import java.util.List;

public interface ProjectDtoRepository extends JpaRepository<ProjectDTO, String> {

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId and e.name = :name")
    @Nullable ProjectDTO findByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId")
    @Nullable List<ProjectDTO> findByIndex(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId = :userId and e.name = :name")
    void removeByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Modifying
    @Query("DELETE FROM ProjectDTO e")
    void clear();

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
    void clear(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId and e.id = :id")
    @Nullable ProjectDTO findById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId = :userId and e.id = :id")
    void removeById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId")
    @Nullable List<ProjectDTO> findAllById(
            @Param("userId") @NotNull String userId
    );

}
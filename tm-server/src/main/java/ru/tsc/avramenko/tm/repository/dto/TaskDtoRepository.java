package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public interface TaskDtoRepository extends JpaRepository<TaskDTO, String> {

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId and e.projectId = :projectId")
    @Nullable List<TaskDTO> findAllTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId and e.name = :name")
    @Nullable TaskDTO findByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId")
    @Nullable List<TaskDTO> findByIndex(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.userId = :userId and e.name = :name")
    void removeByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Modifying
    @Query("DELETE FROM TaskDTO e")
    void clear();

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.userId = :userId")
    void clear(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId and e.id = :id")
    @Nullable TaskDTO findById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.userId = :userId and e.id = :id")
    void removeById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId")
    @Nullable List<TaskDTO> findAllById(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("UPDATE TaskDTO e SET e.projectId = NULL WHERE e.userId = :userId AND e.projectId = :projectId")
    void unbindAllTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

}
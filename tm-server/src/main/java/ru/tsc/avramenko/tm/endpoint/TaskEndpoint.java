package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.avramenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.api.service.dto.ITaskDtoService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Autowired
    private ITaskDtoService taskDtoService;

    @Autowired
    private IProjectTaskDtoService projectTaskDtoService;

    @Override
    public TaskDTO bindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId
    ) {
        sessionDtoService.validate(session);
        return projectTaskDtoService.bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    public void createTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionDtoService.validate(session);
        taskDtoService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionDtoService.validate(session);
        taskDtoService.clear(session.getUserId());
    }

    @Override
    @Nullable
    public List<TaskDTO> findTaskAll(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.findById(session.getUserId(), id);
    }

    @Override
    @Nullable
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    public List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) {
        sessionDtoService.validate(session);
        return projectTaskDtoService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    public TaskDTO finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.finishById(session.getUserId(), id);
    }

    @Override
    public TaskDTO finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.finishByIndex(session.getUserId(), index);
    }

    @Override
    public TaskDTO finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.finishByName(session.getUserId(), name);
    }

    @Override
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        taskDtoService.removeById(session.getUserId(), id);
    }

    @Override
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        taskDtoService.removeByIndex(session.getUserId(), index);
    }

    @Override
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        taskDtoService.removeByName(session.getUserId(), name);
    }

    @Override
    public TaskDTO changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    public TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    public TaskDTO changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    public TaskDTO startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.startById(session.getUserId(), id);
    }

    @Override
    public TaskDTO startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.startByIndex(session.getUserId(), index);
    }

    @Override
    public TaskDTO startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.startByName(session.getUserId(), name);
    }

    @Override
    public TaskDTO unbindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId
    ) {
        sessionDtoService.validate(session);
        return projectTaskDtoService.unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.updateById(session.getUserId(), taskId, name, description);
    }

    @Override
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionDtoService.validate(session);
        return taskDtoService.updateByIndex(session.getUserId(), index, name, description);
    }

}
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project Create</h1>

<form:form action="/project/create/?" method="POST" modelAttribute="project">
<form:input type="hidden" path="id"/>

<form:input type="hidden" path="userId"/>

<p>
<div>Name:</div>
<div><form:input type="text" path="name"/></div>

<p>
<div>Description:</div>
<div><form:input type="text" path="description"/></div>

<p>
<div>Status:</div>
<div><form:select path="status">
    <form:options items="${statuses}" itemLabel="displayName"/>
</form:select></div>

<p>
<div>Start Date:</div>
<div><form:input type="datetime-local" path="startDate"/></div>

<p>
<div>Finish Date:</div>
<div><form:input type="datetime-local" path="finishDate"/></div>

<button type="submit">Create Project</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>
package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.IUserEndpoint;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public void create(
            @WebParam(name = "login")
            @PathVariable("login") @NotNull final String login,
            @WebParam(name = "password")
            @PathVariable("login") @NotNull final String password
    ) {
        userService.create(login, password);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteByLogin(
            @WebParam(name = "login")
            @PathVariable("login") @NotNull final String login
    ) {
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public User find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "user")
            @RequestBody @NotNull final User user
    ) {
        userService.save(user);
    }

}